package passive;

import java.util.List;

public interface Model {

  void regenerateNumberList(int amount);
  String getStat();

}
