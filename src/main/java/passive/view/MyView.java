package passive.view;


import passive.Domain;
import passive.conrtoller.Controller;
import passive.conrtoller.ControllerImpl;
import passive.flower.*;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;


public class MyView {

  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);

  public MyView() {
    controller = new ControllerImpl();
    menu = new LinkedHashMap<>();
    menu.put("1", "  1 - print FlowerList");
    menu.put("2", "  2 - Add");
    menu.put("3", "  3 - print cost for all");
    menu.put("4", "  4 - Your bouquet");
    menu.put("Q", "  Q - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);
    methodsMenu.put("3", this::pressButton3);
    methodsMenu.put("3", this::pressButton4);
  }

  private void pressButton1() {
    System.out.println(controller.getFlowerList(
    ));
  }

  private void pressButton2() {
    System.out.print("What you want to find?");
    String amount = input.nextLine();
    Bouquet bouquet = new Bouquet();
    switch (amount) {
      case "Rose":
        bouquet.addFlowerToList(new Rose(40));
        break;
      case "Gerbera":
        bouquet.addFlowerToList(new Gerbera(30));
        break;
      case "Lilac":
        bouquet.addFlowerToList(new Lilac(50));
        break;
      case "Lotus":
        bouquet.addFlowerToList(new Lotus(25));
        break;
      default:
        System.out.println("That flower is absence");
        break;
    }



    }





  private void pressButton3() {
    System.out.println(controller.getStat());
  }

  private void pressButton4(){
    Bouquet bouquet = new Bouquet();
    System.out.println(bouquet.getFlowerList());
  }

  //-------------------------------------------------------------------------

  private void outputMenu() {
    System.out.println("\nMENU:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }
}
